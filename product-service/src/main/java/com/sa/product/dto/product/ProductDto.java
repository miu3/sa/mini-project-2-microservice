package com.sa.product.dto.product;

import com.sa.product.model.Product;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class ProductDto {

    private Long id;
    private @NotNull String name;
    private @NotNull double price;
    private @NotNull String vendor;
    private @NotNull Integer quantity;
    private @NotNull Long categoryId;

    public ProductDto(Product product) {
        this.setId(product.getId());
        this.setName(product.getName());
        this.setQuantity(product.getQuantity());
        this.setVendor(product.getVendor());
        this.setPrice(product.getPrice());
        this.setCategoryId(product.getCategory().getId());
    }
}
