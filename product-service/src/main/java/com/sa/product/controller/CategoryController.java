package com.sa.product.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.sa.product.model.Category;
import com.sa.product.service.CategoryService;

@RestController
@RequestMapping("/category")

public class CategoryController {

	@Autowired
	private CategoryService categoryService;

	@GetMapping("/")
	public ResponseEntity<List<Category>> getCategories() {
		List<Category> body = categoryService.listCategories();
		return new ResponseEntity<List<Category>>(body, HttpStatus.OK);
	}

	@PostMapping("/create")
	public ResponseEntity<?> createCategory(@Valid @RequestBody Category category) {
		categoryService.createCategory(category);
		return new ResponseEntity<>("created the category", HttpStatus.CREATED);
	}

	@PostMapping("/update/{categoryID}")
	public ResponseEntity<?> updateCategory(@PathVariable("categoryID") long categoryID,
			@Valid @RequestBody Category category) {
		categoryService.updateCategory(categoryID, category);
		return new ResponseEntity<>("updated the category", HttpStatus.OK);
	}
}
