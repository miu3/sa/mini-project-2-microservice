package com.sa.product.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sa.product.dto.product.ProductDto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@NoArgsConstructor
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private @NotNull String name;
    private @NotNull String vendor;
    private @NotNull Integer quantity;
    private @NotNull double price;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "category_id", nullable = false)
    Category category;

    public Product(ProductDto productDto, Category category) {
        this.name = productDto.getName();
        this.vendor = productDto.getVendor();
        this.quantity = productDto.getQuantity();
        this.price = productDto.getPrice();
        this.category = category;
    }
}
