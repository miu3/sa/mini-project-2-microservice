package com.sa.order.service;

// import com.stripe.Stripe;
// import com.stripe.exception.StripeException;
// import com.stripe.model.checkout.Session;
// import com.stripe.param.checkout.SessionCreateParams;
import com.sa.order.dto.cart.CartDto;
import com.sa.order.dto.cart.CartItemDto;
import com.sa.order.dto.order.OrderDto;
import com.sa.order.dto.order.PlaceOrderDto;
import com.sa.order.exceptions.OrderNotFoundException;
import com.sa.order.model.*;
import com.sa.order.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private CartService cartService;

    @Autowired
    OrderItemsService orderItemsService;

    public int saveOrder(PlaceOrderDto orderDto) {
        Order order = getOrderFromDto(orderDto);
        return orderRepository.save(order).getId();
    }

    private Order getOrderFromDto(PlaceOrderDto orderDto) {
        Order order = new Order(orderDto);
        return order;
    }

    public List<Order> listOrders() {
        List<Order> orderList = orderRepository.findAll();
        return orderList;
    }

    public Order getOrder(int orderId) throws OrderNotFoundException {
        Optional<Order> order = orderRepository.findById(orderId);
        if (order.isPresent()) {
            return order.get();
        }
        throw new OrderNotFoundException("Order not found");
    }

    public static OrderDto getDtoFromOrder(Order order) {
        OrderDto orderDto = new OrderDto(order);
        return orderDto;
    }

    public void placeOrder() {
        CartDto cartDto = cartService.listCartItems();

        PlaceOrderDto placeOrderDto = new PlaceOrderDto();
        placeOrderDto.setTotalPrice(cartDto.getTotalCost());

        int orderId = saveOrder(placeOrderDto);
        List<CartItemDto> cartItemDtoList = cartDto.getCartItems();
        for (CartItemDto cartItemDto : cartItemDtoList) {
            OrderItem orderItem = new OrderItem(
                    orderId,
                    cartItemDto.getProductId(),
                    cartItemDto.getQuantity(),
                    cartItemDto.getProductPrice());

            orderItemsService.addOrderedProducts(orderItem);
        }
        cartService.deleteCartItems();
    }

    public Integer getOrderById(Integer orderId) throws OrderNotFoundException {
        Optional<Order> optionalOrder = orderRepository.findById(orderId);
        if (!optionalOrder.isPresent())
            throw new OrderNotFoundException("Order id is invalid " + orderId);
        return optionalOrder.get().getId();
    }

    public Double getTotalPrice(Integer orderId) throws OrderNotFoundException {
        Optional<Order> optionalOrder = orderRepository.findById(orderId);
        if (!optionalOrder.isPresent())
            throw new OrderNotFoundException("Order id is invalid " + orderId);
        return optionalOrder.get().getTotalPrice();
    }
}
