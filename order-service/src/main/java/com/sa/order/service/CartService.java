package com.sa.order.service;

import com.sa.order.dto.cart.AddToCartDto;
import com.sa.order.dto.cart.CartDto;
import com.sa.order.dto.cart.CartItemDto;
import com.sa.order.exceptions.CartItemNotExistException;
import com.sa.order.model.*;
import com.sa.order.repository.CartRepository;
import com.sa.order.request.ProductServiceRequest;

import lombok.NoArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
@Transactional
public class CartService {

    @Autowired
    private CartRepository cartRepository;
    @Autowired
    private ProductServiceRequest productServiceRequest;

    public CartService(CartRepository cartRepository) {
        this.cartRepository = cartRepository;
    }

    public void addToCart(AddToCartDto addToCartDto) {
        Cart cart = getAddToCartFromDto(addToCartDto);
        cartRepository.save(cart);
    }

    private Cart getAddToCartFromDto(AddToCartDto addToCartDto) {
        Cart cart = new Cart(addToCartDto);
        return cart;
    }

    public CartDto listCartItems() {
        List<Cart> cartList = cartRepository.findAll();
        List<CartItemDto> cartItems = new ArrayList<>();
        for (Cart cart : cartList) {
            CartItemDto cartItemDto = getDtoFromCart(cart);
            cartItems.add(cartItemDto);
        }

        double totalCost = 0;
        for (CartItemDto cartItemDto : cartItems) {
            totalCost += (cartItemDto.getProductPrice() * cartItemDto.getQuantity());
        }
        CartDto cartDto = new CartDto(cartItems, totalCost);
        return cartDto;
    }

    public static CartItemDto getDtoFromCart(Cart cart) {
        CartItemDto cartItemDto = new CartItemDto(cart);
        return cartItemDto;
    }

    public void deleteCartItems() {
        cartRepository.deleteAll();
    }
}
