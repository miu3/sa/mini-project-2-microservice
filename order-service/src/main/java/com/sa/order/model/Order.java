package com.sa.order.model;

import lombok.Data;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.sa.order.dto.order.PlaceOrderDto;

import java.util.Date;
import java.util.List;

@Entity
@Data
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "created_date")
    private Date createdDate;
    @Column(name = "total_price")
    private Double totalPrice;

    @JsonManagedReference
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "order_id", referencedColumnName = "id", insertable = false, updatable = false)
    private List<OrderItem> orderItems;

    public Order() {
    }

    public Order(PlaceOrderDto orderDto) {
        this.totalPrice = orderDto.getTotalPrice();
        this.createdDate = new Date();
    }
}
