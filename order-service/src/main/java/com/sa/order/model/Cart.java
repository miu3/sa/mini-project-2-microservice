package com.sa.order.model;

import com.sa.order.dto.cart.AddToCartDto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@Table(name = "cart")
public class Cart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "product_id")
    private @NotBlank Long productId;

    @Column(name = "created_date")
    private Date createdDate;

    private int quantity;
    private double productPrice;

    public Cart(AddToCartDto addToCartDto) {
        this.productId = addToCartDto.getProductId();
        this.quantity = addToCartDto.getQuantity();
        this.productPrice = addToCartDto.getProductPrice();
        this.createdDate = new Date();
    }
}
