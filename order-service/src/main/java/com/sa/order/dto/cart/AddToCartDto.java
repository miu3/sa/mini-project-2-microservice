package com.sa.order.dto.cart;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class AddToCartDto {
    private Integer id;
    private @NotNull Long productId;
    private @NotNull Double productPrice;
    private @NotNull Integer quantity;
}