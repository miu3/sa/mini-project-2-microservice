package com.sa.order.dto.cart;

import com.sa.order.model.Cart;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CartItemDto {
    private Integer id;
    private @NotNull Integer quantity;
    private @NotNull Long productId;
    private @NotNull double productPrice;

    public CartItemDto(Cart cart) {
        this.setId(cart.getId());
        this.setProductId(cart.getProductId());
        this.setProductPrice(cart.getProductPrice());
        this.setQuantity(cart.getQuantity());
    }
}
