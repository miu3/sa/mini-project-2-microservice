package com.sa.order.dto.order;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class OrderItemsDto {

    private @NotNull double prodcutPrice;
    private @NotNull int quantity;
    private @NotNull int orderId;
    private @NotNull int productId;

}
