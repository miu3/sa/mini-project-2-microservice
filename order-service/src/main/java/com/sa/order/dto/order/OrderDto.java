package com.sa.order.dto.order;

import com.sa.order.model.Order;

import lombok.Data;

@Data
public class OrderDto {
    private Integer id;

    public OrderDto(Order order) {
        this.setId(order.getId());
    }
}
