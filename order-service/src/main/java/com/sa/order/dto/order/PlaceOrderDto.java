package com.sa.order.dto.order;

import javax.validation.constraints.NotNull;

import com.sa.order.model.Order;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PlaceOrderDto {
    private Integer id;
    private @NotNull Double totalPrice;

    public PlaceOrderDto(Order order) {
        this.setId(order.getId());
        this.setTotalPrice(order.getTotalPrice());

    }
}
