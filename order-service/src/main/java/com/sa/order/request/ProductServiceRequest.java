package com.sa.order.request;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(value = "product-service", path = "/product"

)
public interface ProductServiceRequest {
    @GetMapping("{productId}")
    Long getProductById(@PathVariable("productId") Long productId);

    @GetMapping("{productId}")
    Double getProductPrice(@PathVariable("productId") Long productId);
}
