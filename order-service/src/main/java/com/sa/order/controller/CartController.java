package com.sa.order.controller;

import com.sa.order.dto.cart.AddToCartDto;
import com.sa.order.dto.cart.CartDto;
import com.sa.order.exceptions.FeignExceptionHandler;
import com.sa.order.exceptions.ProductNotFoundException;
import com.sa.order.request.ProductServiceRequest;
import com.sa.order.service.CartService;

import feign.FeignException.FeignServerException;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cart")
public class CartController {
    @Autowired
    private CartService cartService;
    @Autowired
    private ProductServiceRequest productServiceRequest;

    @PostMapping("/add")
    public ResponseEntity<?> addToCart(@RequestBody AddToCartDto addToCartDto) throws ProductNotFoundException {
        // productServiceRequest.getProductById(productId);

        cartService.addToCart(addToCartDto);
        return new ResponseEntity<>("Added to cart", HttpStatus.CREATED);
    }

    @GetMapping("/")
    public ResponseEntity<CartDto> getCartItems() {
        CartDto cartDto = cartService.listCartItems();
        return new ResponseEntity<CartDto>(cartDto, HttpStatus.OK);
    }
}
