# Mini project 2 estore 
## Stack
- Spring boot
- MySQL
- Docker
- Minikube k8s
- Vault secret management

## Services
- Eureka discovery service
- Config server
- Zuul api gateway
- Estore services
    - Category
    - Product
    - Account
    - Order
    - Payment
    - Transaction
    - Shipping