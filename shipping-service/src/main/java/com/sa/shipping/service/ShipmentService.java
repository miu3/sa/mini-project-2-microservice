package com.sa.shipping.service;

import com.sa.shipping.dto.ShipmentDto;
import com.sa.shipping.model.Shipment;

import java.util.List;

public interface ShipmentService {
    List<ShipmentDto> findAll();

    Shipment addShipment(ShipmentDto shipmentDto);

    Shipment updateShipment(Long shipmentId, ShipmentDto shipmentDto);
}
