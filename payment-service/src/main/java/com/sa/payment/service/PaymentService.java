package com.sa.payment.service;

import com.sa.payment.dto.PaymentDto;
import com.sa.payment.model.Payment;

import java.util.List;

public interface PaymentService {
    List<PaymentDto> findAll();

    Payment addPayment(PaymentDto paymentDto);

    Payment payPayment(Long paymentId, PaymentDto paymentDto);

    Payment getPayment(Long paymentId);

}
