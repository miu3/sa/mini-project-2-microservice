package com.sa.payment.service;

import com.sa.payment.dto.PaymentDto;
import com.sa.payment.model.Payment;
import com.sa.payment.model.PaymentStatus;
import com.sa.payment.model.PaymentType;
import com.sa.payment.repository.PaymentRepository;
import com.sa.payment.request.OrderServiceRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    private final PaymentRepository paymentRepository;

    public static PaymentDto getPaymentDtoFromPayment(Payment payment) {
        PaymentDto paymentDto = new PaymentDto(payment);
        return paymentDto;
    }

    @Override
    public List<PaymentDto> findAll() {
        List<Payment> payments = paymentRepository.findAll();
        List<PaymentDto> paymentDtos = new ArrayList<>();
        for (Payment payment : payments) {
            PaymentDto paymentDto = getPaymentDtoFromPayment(payment);
            paymentDtos.add(paymentDto);
        }
        return paymentDtos;
    }

    public static Payment getPaymentFromDto(PaymentDto paymentDto) {
        Payment payment = new Payment(paymentDto);
        return payment;
    }

    @Override
    public Payment addPayment(PaymentDto paymentDto) {
        Payment payment = getPaymentFromDto(paymentDto);
        payment.setPaymentTime(LocalDateTime.now());
        payment.setIsPayed(false);
        payment.setPaymentType(PaymentType.PAYPAL);
        payment.setPaymentStatus(PaymentStatus.IN_PROGRESS);
        return paymentRepository.save(payment);
    }

    @Override
    public Payment payPayment(Long paymentId, PaymentDto paymentDto) {
        Payment payment = getPaymentFromDto(paymentDto);
        payment.setId(paymentId);
        payment.setIsPayed(true);
        payment.setPaymentStatus(PaymentStatus.COMPLETED);
        return paymentRepository.save(payment);
    }

    @Override
    public Payment getPayment(Long paymentId) {
        Optional<Payment> optionalPayment = paymentRepository.findById(paymentId);
        return optionalPayment.get();
    }

}
