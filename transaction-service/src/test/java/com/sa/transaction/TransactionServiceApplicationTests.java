package com.sa.transaction;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootTest
@EnableFeignClients
class TransactionServiceApplicationTests {

	@Test
	void contextLoads() {
	}

}
