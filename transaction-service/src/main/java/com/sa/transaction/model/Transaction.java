package com.sa.transaction.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "transactions")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "payment_id")
    private Integer paymentId;

    @Column(name = "order_id")
    private Integer orderId;

    @Column(name = "shipping_id")
    private Integer shippingId;

    @Column(name = "transaction_status")
    private String transactionStatus;
}
