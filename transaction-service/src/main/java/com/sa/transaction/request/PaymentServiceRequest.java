package com.sa.transaction.request;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(value = "payment-service", path = "api/pyament"

)
public interface PaymentServiceRequest {
    @GetMapping("{paymentId}")
    Long getPayment(@PathVariable("paymentId") Long paymentId);
}
