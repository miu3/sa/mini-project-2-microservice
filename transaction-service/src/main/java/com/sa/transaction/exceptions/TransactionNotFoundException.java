package com.sa.transaction.exceptions;

public class TransactionNotFoundException extends IllegalArgumentException {
    public TransactionNotFoundException(String msg) {
        super(msg);
    }
}
