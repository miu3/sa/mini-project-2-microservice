package com.sa.transaction.controller;

import com.sa.transaction.exceptions.TransactionNotFoundException;
import com.sa.transaction.model.*;
import com.sa.transaction.service.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/transcation")
public class TransactionController {
    @Autowired
    private TransactionService transactionService;

    @PostMapping("/add")
    public ResponseEntity<?> addTransaction(@RequestBody Transaction transaction) {
        transactionService.saveTransaction(transaction);
        ;
        return new ResponseEntity<>("Transaction created", HttpStatus.CREATED);
    }

    @GetMapping("/")
    public ResponseEntity<List<Transaction>> getAllTransactions() {
        List<Transaction> transactions = transactionService.listTransaction();
        return new ResponseEntity<List<Transaction>>(transactions, HttpStatus.OK);
    }

}
