package com.sa.transaction.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TransactionDTO {
    private Integer id;
    private Integer paymentId;
    private Integer orderId;
    private Integer shippingId;
    private String transactionStatus;
}
