package com.sa.transaction.service;

import com.sa.transaction.exceptions.TransactionNotFoundException;
import com.sa.transaction.model.*;
import com.sa.transaction.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;

    public void saveTransaction(Transaction transaction) {
        transactionRepository.save(transaction);
    }

    public List<Transaction> listTransaction() {
        return transactionRepository.findAll();
    }

}
